# This file exports every derivation introduced by this repository.
{ nixpkgs ? import ./nixpkgs.nix }:
let pkgs = import ./pkgs.nix { inherit nixpkgs; };
in
pkgs.releaseTools.aggregate {
  name = "everything";
  constituents = [
    pkgs._here.ghc861.moto
    pkgs._here.ghc861.moto.doc
    pkgs._here.ghc861.moto-example
    pkgs._here.ghc861.moto-postgresql
    pkgs._here.ghc861.moto-postgresql.doc
    pkgs._here.ghc861._shell
  ];
}

