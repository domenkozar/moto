{ pkgs }:

# To be used as `packageSetConfig` for a Haskell pacakge set:
let
src_di =
  builtins.fetchGit {
    url = "https://github.com/k0001/di";
    rev = "edf3a496a8f529ab9047da5d5bfed51d38a77cca";
  };

contravariant =
  { mkDerivation, base, StateVar, stdenv, transformers }:
  mkDerivation {
    pname = "contravariant";
    version = "1.5";
    sha256 = "6ef067b692ad69ffff294b953aa85f3ded459d4ae133c37896222a09280fc3c2";
    libraryHaskellDepends = [ base StateVar transformers ];
    homepage = "http://github.com/ekmett/contravariant/";
    description = "Contravariant functors";
    license = stdenv.lib.licenses.bsd3;
  };
base-orphans =
  { mkDerivation, base, ghc-prim, hspec, hspec-discover, QuickCheck
  , stdenv
  }:
  mkDerivation {
    pname = "base-orphans";
    version = "0.8";
    sha256 = "aceec656bfb4222ad3035c3d87d80130b42b595b72888f9ab59c6dbb7ed24817";
    libraryHaskellDepends = [ base ghc-prim ];
    testHaskellDepends = [ base hspec QuickCheck ];
    testToolDepends = [ hspec-discover ];
    homepage = "https://github.com/haskell-compat/base-orphans#readme";
    description = "Backwards-compatible orphan instances for base";
    license = stdenv.lib.licenses.mit;
  };
unordered-containers =
  { mkDerivation, base, bytestring, ChasingBottoms, containers
  , criterion, deepseq, deepseq-generics, fetchgit, hashable, hashmap
  , HUnit, mtl, QuickCheck, random, stdenv, test-framework
  , test-framework-hunit, test-framework-quickcheck2
  }:
  mkDerivation {
    pname = "unordered-containers";
    version = "0.2.9.0";
    src = fetchgit {
      url = "https://github.com/tibbe/unordered-containers";
      sha256 = "1900jzqri2b64fxj5rgm5xj6yc3w50z6cp2fjxdnwd89mxsfbkfh";
      rev = "efa43a2ab09dc6eb72893d12676a8e188cb4ca63";
    };
    libraryHaskellDepends = [ base deepseq hashable ];
    testHaskellDepends = [
      base ChasingBottoms containers hashable HUnit QuickCheck
      test-framework test-framework-hunit test-framework-quickcheck2
    ];
    benchmarkHaskellDepends = [
      base bytestring containers criterion deepseq deepseq-generics
      hashable hashmap mtl random
    ];
    homepage = "https://github.com/tibbe/unordered-containers";
    description = "Efficient hashing-based container types";
    license = stdenv.lib.licenses.bsd3;
    jailbreak = true;
  };
stringsearch =
  { mkDerivation, array, base, bytestring, containers, stdenv }:
  mkDerivation {
    pname = "stringsearch";
    version = "0.3.6.6";
    sha256 = "295f1971920bc52263d8275d7054ad223a7e1aefe75533f9887735c9644ffe4a";
    libraryHaskellDepends = [ array base bytestring containers ];
    homepage = "https://bitbucket.org/dafis/stringsearch";
    description = "Fast searching, splitting and replacing of ByteStrings";
    license = stdenv.lib.licenses.bsd3;
    patches = [ ./stringsearch.cabal.patch ];
  };
semigroupoids =
  { mkDerivation, base, base-orphans, bifunctors, Cabal
  , cabal-doctest, comonad, containers, contravariant, distributive
  , doctest, hashable, semigroups, stdenv, tagged, template-haskell
  , transformers, transformers-compat, unordered-containers
  }:
  mkDerivation {
    pname = "semigroupoids";
    version = "5.3.1";
    sha256 = "cd89ec61f86260997c79c09bacb7d6c18031375bc3e5467b36f7cb812793388e";
    setupHaskellDepends = [ base Cabal cabal-doctest ];
    libraryHaskellDepends = [
      base base-orphans bifunctors comonad containers contravariant
      distributive hashable semigroups tagged template-haskell
      transformers transformers-compat unordered-containers
    ];
    testHaskellDepends = [ base doctest ];
    homepage = "http://github.com/ekmett/semigroupoids";
    description = "Semigroupoids: Category sans id";
    license = stdenv.lib.licenses.bsd3;
    jailbreak = true;
  };
profunctors =
  { mkDerivation, base, base-orphans, bifunctors, comonad
  , contravariant, distributive, semigroups, stdenv, tagged
  , transformers
  }:
  mkDerivation {
    pname = "profunctors";
    version = "5.3";
    sha256 = "74632acc5bb76e04ade95e187be432b607da0e863c0e08f3cabafb23d8b4a3b7";
    libraryHaskellDepends = [
      base base-orphans bifunctors comonad contravariant distributive
      semigroups tagged transformers
    ];
    homepage = "http://github.com/ekmett/profunctors/";
    description = "Profunctors";
    license = stdenv.lib.licenses.bsd3;
  };

inherit (pkgs.haskell.lib) dontCheck doJailbreak;

in
pkgs.lib.composeExtensions
  (import "${src_di}/hs-overlay.nix" { inherit pkgs; })
  (self: super: {
    base-orphans = super.callPackage base-orphans {};
    ChasingBottoms = dontCheck super.ChasingBottoms;
    contravariant = super.callPackage contravariant {};
    doctest = doJailbreak super.doctest;
    free = doJailbreak super.free;
    hackage-security = doJailbreak super.hackage-security;
    HTTP = doJailbreak super.HTTP;
    lens-family-core = doJailbreak super.lens-family-core;
    pipes-group = doJailbreak super.pipes-group;
    profunctors = super.callPackage profunctors {};
    semigroupoids = super.callPackage semigroupoids {};
    stringsearch = super.callPackage stringsearch {};
    unordered-containers = super.callPackage unordered-containers {};

    moto = super.callPackage ./moto/pkg.nix {};
    moto-example = super.callPackage ./moto-example/pkg.nix {};
    moto-postgresql = super.callPackage ./moto-postgresql/pkg.nix {};

    _shell = self.shellFor {
      withHoogle = false; # hoogle dependencies don't compile
      nativeBuildInputs = [ self.cabal-install ];
      packages = p: [
        p.moto
        p.moto-example
        p.moto-postgresql
      ];
    };
  })
