{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PartialTypeSignatures #-}

module Main where

import qualified Di
import qualified Moto
import qualified Moto.File

--------------------------------------------------------------------------------

emptyBackup :: Monoid pre => Moto.Backup pre
emptyBackup = Moto.Backup (\_ k -> k mempty)

emptyStore :: Monoid pre => Moto.Store pre
emptyStore = Moto.Store (\_ _ _ -> pure ())
                        (\_ _ k -> k mempty)
                        (\_ _ -> pure ())

emptyChange :: Moto.Change pre
emptyChange = Moto.Change (\_ _ _ _ -> pure ())

emptyMig :: Moto.Mig id deps
emptyMig = Moto.Mig emptyStore emptyBackup (emptyChange :: Moto.Change ())

--------------------------------------------------------------------------------

mig_red :: Moto.Mig "red" '["blue"]
mig_red = emptyMig

mig_yellow :: Moto.Mig "yellow" '["black", "red"]
mig_yellow = emptyMig

mig_green :: Moto.Mig "green" '["red"]
mig_green = emptyMig

mig_black :: Moto.Mig "black" '["blue"]
mig_black = emptyMig

mig_blue :: Moto.Mig "blue" '[]
mig_blue = emptyMig

migs :: Moto.Migs _
migs = Moto.migs
  Moto.* mig_blue
  Moto.* mig_red
  Moto.* mig_black
  Moto.* mig_green
  Moto.* mig_yellow

main :: IO ()
main = Di.new $ \di -> do
  (opts, ()) <- Moto.getOpts Moto.File.registryConf (pure ())
  Moto.run di migs opts
