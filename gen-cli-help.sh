#! /usr/bin/env nix-shell
#! nix-shell ./shell.nix -i bash

set -e
OUT=./moto/lib/cli_help.docs

rm -rf ./dist-newstyle
cabal new-configure all
echo "" >$OUT
cabal new-build moto-example

BIN=./dist-newstyle/build/x86_64-linux/ghc-8.6.0.20180810/moto-example-0.0.1/x/moto-example/build/moto-example/moto-example
PROG=moto-example
SUBS="run show-migrations check-migrations show-registry clean-registry \
      delete-recovery-data"

echo "{- \$cli_help" >$OUT
echo >>$OUT
echo "This is the full description of the command line options supported by" >>$OUT
echo "'Moto.getOpts', using @Moto.File.fileRegistry@ as the registry." >>$OUT
echo >>$OUT
echo "Main program (here called @$PROG@):" >>$OUT
echo >>$OUT
echo "@" >>$OUT
$BIN --help | sed 's,/,\\/,g' >>$OUT
echo "@" >>$OUT
for s in $SUBS; do
  echo >>$OUT
  echo -e "\n\nSubcommand @$s@:" >>$OUT
  echo >>$OUT
  echo "@" >>$OUT
  $BIN $s --help | sed 's,/,\\/,g' >>$OUT
  echo "@" >>$OUT
done
echo "-}" >>$OUT

rm -rf ./dist-newstyle
cabal new-haddock all
