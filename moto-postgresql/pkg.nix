{ mkDerivation, base, bytestring, moto, postgresql-simple
, safe-exceptions, stdenv
}:
mkDerivation {
  pname = "moto-postgresql";
  version = "0.0.1";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring moto postgresql-simple safe-exceptions
  ];
  description = "PostgreSQL-based migrations registry for moto";
  license = stdenv.lib.licenses.asl20;
}
