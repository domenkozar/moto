# Version 0.0.3

* Haddocks and Hackage choke on internal Cabal libraries, so we remove the
  internal `moto-internal` library.


# Version 0.0.2

* Added file missing from distribution.


# Version 0.0.1

* Initial version.
